<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>
		<section class="block-annonce-single">
			<div class="container-single-annonce">

				<div class="photo-titre-prix-auteur-category-annonce-single">
					
					<div class="photos-gallery-annonce-single">
						<img class="photo-gallery-annonce-single" src="img/iconphoto.png">
						<img class="photo-gallery-annonce-single" src="img/iconphoto2.png">			
					</div>

					<div class="btn-gallery-annonce-single">

						<button class="ui left labeled icon button my-arrow-left">
							<i class="left arrow icon"></i>
							Précédent
						</button>

						<button class="ui right labeled icon button my-arrow-right">
							<i class="right arrow icon"></i>
							Suivant
						</button>
					</div>					
					
					<div class="titre-prix-auteur-category-annonce-single">
						<h2 class="titre-annonce-single">Titre de l'annonce</h2>
						<p class="prix-annonce-single">Prix de l'annonce € / jour</p>
						<p class="category-annonce-single">Catégorie vélo</p>
						<p class="auteur-annonce-single">Annonce posté par prénom</p>
					</div>
				</div>

				<div class="reservation-annonce-single border-annonce-single">
					<h2>Réservation</h2>

					<form class="form-reservation-annonce-single" action="confirmation.php">
						<div class="ui action big input input-datepicker">
							<input id="datepicker-annonce" type="text" placeholder="jj/mm/aaaa" data-toggle="datepicker">
							<label for="datepicker-annonce" class="ui icon button">
								<i class="calendar alternate outline icon"></i>
							</label>
						</div>

						<button type="submit" class="btn-resvation-annonce-single positive ui button big">Réserver</button>
					</form>
				</div>

				<div class="description-annonce-single border-annonce-single">
					<h2>Description</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio animi molestias non impedit nesciunt at nulla! Doloremque asperiores nobis architecto voluptate hic eaque eos aperiam veritatis dolorum, rerum, adipisci voluptatibus aut ut odio tempora nostrum a, in quaerat voluptatum repellat.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam aliquid optio enim, possimus repellat nostrum porro odio cum perspiciatis omnis.</p>
				</div>

				<div class="localisation-annonce-single border-annonce-single">
					<h2>Localisation</h2>
					<iframe class="iframe-localisation-annonce-single" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d167998.10803373056!2d2.206977064368058!3d48.858774103123785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sParis!5e0!3m2!1sfr!2sfr!4v1555059245978!5m2!1sfr!2sfr" height="115" width="1920" frameborder="0"allowfullscreen></iframe>
				</div>

				<div class="border-annonce-single-small block-btn-options-annonce-single">
					<a href="#" class="btn-options-annonce-single"><i class="print icon"></i>Imprimer</a>
					<a href="#" class="btn-options-annonce-single"><i class="envelope icon"></i>Partager par mail</a>
					<a href="#" class="btn-options-annonce-single"><i class="facebook icon"></i>Partager sur facebook</a>
					<a href="signalannonce.php" class="btn-options-annonce-single"><i class="exclamation triangle icon"></i>Signaler un abus</a>
					<a href="removeannonce.php" class="btn-options-annonce-single"><i class="trash alternate outline icon"></i>Supprimer l'annonce</a>
				</div>

				

			</div>
			
		</section>
	</main>

	<?php require('footer.php'); ?>

	<script type="text/javascript">
		$('.photos-gallery-annonce-single').slick();

		$('.my-arrow-left').click(function(){
			$('.photos-gallery-annonce-single').slick('slickPrev');
		})

		$('.my-arrow-right').click(function(){
			$('.photos-gallery-annonce-single').slick('slickNext');
		})

		$('[data-toggle="datepicker"]').datepicker({
			format: 'dd/mm/yyyy'
		});
	</script>

</body>
</html>