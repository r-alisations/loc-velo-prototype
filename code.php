<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>

		<section class="code-paiement">
			<div class="container">
				<div class="block-form-code-paiement block-part-form">
					
					<h1 class="ui dividing header">Saisissez le code de paiement</h1>

					<form class="ui form" action="codemessage.php">

						<div class="field">
							<label>Code *</label>
							<input type="text" name="code" placeholder="Code">
						</div>

						<button class="ui button positive" type="submit">Valider</button>

					</form>
				</div>
			</div>
		</section>

	</main>

</body>
</html>