<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>
		<section class="paiement-confirmation">
			<div class="container">

				<div class="block-message block-message-confirmation">

					<img class="image-confirmation" src="img/confirmation.png" alt="confirmation">
					
					<div class="block-text-message">
						<h2 class="titre-confirmation">Félicitations, votre code de paiement a été accepté !</h2>

						<p class="paragraphe-confirmation">
							Nous vous remercions de votre confiance. Vous recevrez le paiement dans les plus brefs délais.
						</p>	
					</div>
					
					<div class="block-btn-message">
						<a class="ui primary basic button large" href="home.php">Retour à l'accueil</a>
					</div>
					
				</div>

			</div>
		</section>
	</main>

</body>
</html>