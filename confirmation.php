<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>
		<section class="reservation-confirmation">
			<div class="container">

				<div class="block-message block-message-confirmation">

					<img class="image-confirmation" src="img/confirmation.png" alt="confirmation">
					
					<div class="block-text-message">
						<h2 class="titre-confirmation">Félicitations, vous avez réservé le vélo de Prénom pour la journée du Date !</h2>

						<p class="paragraphe-confirmation">
							Nous vous remercions de votre confiance. Vous recevrez un email avec les informations de contact du loueur et le code de validation à transmettre au loueur pour qu'il obtienne le paiement.
						</p>	
					</div>
					
					<div class="block-btn-message">
						<a class="ui primary basic button large" href="home.php">Retour à l'accueil</a>
					</div>
					
				</div>

			</div>
		</section>
	</main>

</body>
</html>