<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>

		<section class="remove-annonce">
			<div class="container">
				<div class="block-form-remove-annonce block-part-form">
					
					<h1 class="ui dividing header">Contactez nous</h1>

					<form class="ui form" action="contactconfirmation.php">

						<div class="field">
							<label>Nom *</label>
							<input type="text" placeholder="Nom">
						</div>

						<div class="field">
							<label>Prénom *</label>
							<input type="text" placeholder="Prénom">
						</div>

						<div class="field">
							<label>Email *</label>
							<input type="text" placeholder="Email">
						</div>

						<div class="field">
							<label>Message *</label>
							<textarea placeholder="Message"></textarea>
						</div>

						<button class="ui button primary" type="submit">Envoyer</button>

					</form>
				</div>
			</div>
		</section>

	</main>

</body>
</html>