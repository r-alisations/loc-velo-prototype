<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>
		<section class="reservation-confirmation">
			<div class="container">

				<div class="block-message block-message-confirmation">

					<img class="image-confirmation" src="img/confirmation.png" alt="confirmation">
					
					<div class="block-text-message">
						<h2 class="titre-confirmation">Message envoyé !</h2>

						<p class="paragraphe-confirmation">
							Nous avons bien recu votre message. Nous allons vous répondre dans les plus brefs délais.
						</p>	
					</div>
					
					<div class="block-btn-message">
						<a class="ui primary basic button large" href="home.php">Retour à l'accueil</a>
					</div>
					
				</div>

			</div>
		</section>
	</main>

</body>
</html>