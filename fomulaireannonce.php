<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>

		<section class="formulaire">
			<div class="container">
				<form class="ui form" action="validationformulaire.php">

					<div class="block-part-form">

						<h3 class="ui dividing header">Votre annonce</h3>

						<div class="field">
							<label>Catégorie *</label>
							<select class="ui fluid dropdown">
								<option value="" disabled selected>Choisissez une catégorie</option>
								<option value="velo-ville">Vélo de ville</option>
								<option value="velo-electrique">Vélo électrique</option>
								<option value="tandem">Tandem</option>
								<option value="velo-route">Vélo de route</option>
								<option value="vtt">VTT</option>
								<option value="velo-enfant">Vélo enfant</option>
							</select>
						</div>

						<div class="field">
							<label>Titre de l'annonce *</label>
							<input type="text" placeholder="Titre de l'annonce">
						</div>

						<div class="field">
							<label>Description de l'annonce *</label>
							<textarea placeholder="Description de l'annonce"></textarea>
						</div>

						<div class="field">
							<label>Prix journalier *</label>
							<div class="ui icon input">
								<input type="text" placeholder="Prix journalier">
								<i class="euro sign icon"></i>
							</div>
						</div>

						<div class="field">
							<label>Photos</label>
						</div>
						<div class="block-field-input-photos">
							<label for="photo1" class="ui icon button photos-label">
						    	<p class="text-label">Photo principale</p>
								<i class="camera icon camera-label">
									<i class="plus circle icon camera-plus-label"></i>
								</i>
							</label>
							<input type="file" class="input-photos" id="photo1">

							<label for="photo2" class="ui icon button photos-label">
						    	<p class="text-label">Photo 2</p>
								<i class="camera icon camera-label">
									<i class="plus circle icon camera-plus-label"></i>
								</i>
							</label>
							<input type="file" class="input-photos" id="photo2">

							<label for="photo3" class="ui icon button photos-label">
						    	<p class="text-label">Photo 3</p>
								<i class="camera icon camera-label">
									<i class="plus circle icon camera-plus-label"></i>
								</i>
							</label>
							<input type="file" class="input-photos" id="photo3">
						</div>

					</div>   

					
					<div class="block-part-form">

						<h3 class="ui dividing header">Localisation</h3>

						<div class="field">
							<label>Adresse *</label>
							<input type="text" placeholder="Adresse">
						</div>

						<div class="field">
							<label>Ville ou code postal *</label>
							<div class="ui left icon input">
								<input type="text" placeholder="Ville ou code postal">
								<i class="map marker alternate icon icon-marker"></i>
							</div>	
						</div>

					</div>

				
					<div class="block-part-form">

						<h3 class="ui dividing header">Vos informations</h3>

						<div class="field">
							<label>Nom *</label>
							<input type="text" placeholder="Nom">
						</div>

						<div class="field">
							<label>Prénom *</label>
							<input type="text" placeholder="Prénom">
						</div>

						<div class="field">
							<label>Email *</label>
							<input type="text" placeholder="Email">
						</div>

						<div class="field">
							<label>Téléphone *</label>
							<input type="text" placeholder="Téléphone">
						</div>

					</div>

					<p class="champs-obligatoires">Champs obligatoires *</p>

					<button class="ui primary button btn-valider-formulaire-annonce" type="submit">Valider</button>

				</form>
			</div>
		</section>

	</main>

	<?php require('footer.php'); ?>

	<script type="text/javascript">
		$('.ui.dropdown').dropdown();
	</script>

</body>
</html>