<footer class="footer">
	<div class="container-footer">
		<div class="footer-infos">
			<ul class="footer-infos-list">
				<li><a href="#">Conditions d'utilisation</a></li>
				<span> - </span>
				<li><a href="#">Politique de confidentialité</a></li>
				<span> - </span>
				<li><a href="#">Conditions Générales de Vente</a></li>
				<span> - </span>
				<li><a href="#">Mentions Légales</a></li>
				<span> - </span>
				<li><a href="#">Gestion des cookies</a></li>
				<span> - </span>
				<li><a href="contact.php">Nous contacter</a></li>
			</ul>
		</div>
	</div>
</footer>

<script src="https://code.jquery.com/jquery-3.4.0.js" integrity="sha256-DYZMCC8HTC+QDr5QNaIcfR7VSPtcISykd+6eSmBW5qo=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
