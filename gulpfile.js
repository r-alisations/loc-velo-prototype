//PLUGINS
var {src, dest, series, parallel, watch, lastRun} = require('gulp');
var browserSync = require('browser-sync').create();


//PROXY
var proxy = 'localhost/projet7/home.php';


//FILES HTML PHP
var filesPHP = '*.php';
var sourceStylesFilesCSS = '*.css';


//SERVE
function serve ()
{
	browserSync.init({
        proxy: proxy
    });

    watch(filesPHP).on('change', browserSync.reload);
    watch(sourceStylesFilesCSS).on('change', browserSync.reload);
}


//EXPORT
module.exports = 
{
	serve: serve
}