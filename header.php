<header class="header-site">
	<div class="menu-container">
		<div class="menu">

			<a href="home.php" class="menu-logo-link"><img class="menu-logo-img" src="img/logo.png"></a>

			<nav class="menu-nav-header">
				<a class="menu-liens" href="listeannonce.php"><i class="fas fa-search menu-icones"></i><span class="menu-liens-span">Louer un vélo</span class="menu-liens-span"></a>
				<a class="menu-liens" href="fomulaireannonce.php"><i class="fas fa-plus-circle menu-icones"></i><span class="menu-liens-span">Proposer votre vélo</span></a>				
			</nav>
			
		</div>
	</div>
</header>