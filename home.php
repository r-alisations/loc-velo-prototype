<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>

		<section class="block-wallpaper">
			<div class="container-full">
				
				<div class="block-wallpaper-img">
					<img src="img/fond.jpg">
				</div>

				<div class="block-wallpaper-btn">
					<a class="ui blue button wallpaper-btn" href="listeannonce.php">Louer un vélo</a>
					<a class="ui blue button wallpaper-btn" href="fomulaireannonce.php">Proposer votre vélo</a>
				</div>	

			</div>
		</section>
		
		<section class="block-categories-velo">
			<div class="categories-container">

				<div class="block-section-title">
					<h1 class="section-title">Les vélos.</h1>
				</div>
				
				<div class="categories-velo-list">
					
					<div class="card-velo">
						<a href="listeannonce.php" class="card-body">
							<img class="card-velo-photo" src="img/veloville.jpg">
							<h2 class="card-velo-category">Vélo de ville</h2>
						</a>
						<div class="card-footer">
							<p>à partir de X € / jour</p>
							<p>X annonces</p>
						</div>						
					</div>

					<div class="card-velo">
						<a href="listeannonce.php" class="card-body">
							<img class="card-velo-photo" src="img/veloelectrique.jpg">
							<h2 class="card-velo-category">Vélo électrique</h2>
						</a>
						<div class="card-footer">
							<p>à partir de X € / jour</p>
							<p>X annonces</p>
						</div>						
					</div>

					<div class="card-velo">
						<a href="listeannonce.php" class="card-body">
							<img class="card-velo-photo" src="img/tandem.png">
							<h2 class="card-velo-category">Tandem</h2>
						</a>
						<div class="card-footer">
							<p>à partir de X € / jour</p>
							<p>X annonces</p>
						</div>						
					</div>

					<div class="card-velo">
						<a href="listeannonce.php" class="card-body">
							<img class="card-velo-photo" src="img/veloroute.jpg">
							<h2 class="card-velo-category">Vélo de route</h2>
						</a>
						<div class="card-footer">
							<p>à partir de X € / jour</p>
							<p>X annonces</p>
						</div>						
					</div>

					<div class="card-velo">
						<a href="listeannonce.php" class="card-body">
							<img class="card-velo-photo" src="img/vtt.jpg">
							<h2 class="card-velo-category">VTT</h2>
						</a>
						<div class="card-footer">
							<p>à partir de X € / jour</p>
							<p>X annonces</p>
						</div>						
					</div>

					<div class="card-velo">
						<a href="listeannonce.php" class="card-body">
							<img class="card-velo-photo" src="img/veloenfant.jpg">
							<h2 class="card-velo-category">vélo enfant</h2>
						</a>
						<div class="card-footer">
							<p>à partir de X € / jour</p>
							<p>X annonces</p>
						</div>						
					</div>

				</div>

			</div>
		</section>

		<section class="comment-ca-marche">
			<div class="container">

				<div class="block-section-title">
					<h1 class="section-title">Comment ça marche ?</h1>
				</div>

				<div class="block-step-list">

					<div class="block-step-item">
						<div class="block-step-item-logo step-logo-marker"><i class="fas fa-map-marker-alt fas-step fas-step-marker"></i></div>
						<h2 class="block-step-item-title">Trouve ton vélo</h2>
						<p class="block-step-item-description">Grâce au formulaire de recherche, trouve tous les vélos disponibles autour de toi. Choisis celui qui te plaît et fais une demande de réservation à le jour que tu souhaites !</p>
					</div>

					<div class="block-step-item">
						<div class="block-step-item-logo step-logo-euro"><i class="fas fa-euro-sign fas-step fas-step-euro"></i></div>
						<h2 class="block-step-item-title">Réserve ton vélo</h2>
						<p class="block-step-item-description">Effectue le paiement grâce à notre plateforme de paiement sécurisé. Tu receveras ensuite par email les informations de contact du propriétaire et le code de paiement à fournir au propriétaire.</p>
					</div>

					<div class="block-step-item">
						<div class="block-step-item-logo step-logo-bicycle"><i class="fas fa-bicycle fas-step fas-step-bicycle"></i></div>
						<h2 class="block-step-item-title">Récupère ton vélo</h2>
						<p class="block-step-item-description">Une fois la transaction effectuée, sont mis en relation le propriétaire et le locataire afin de fixer un rendez vous pour récupérer le vélo.</p>
					</div>

				</div>
			</div>
		</section>

		<section class="a-propos">
			<div class="container">
				
				<div class="block-section-title">
					<h1 class="section-title">A propos</h1>
				</div>
				

				<div class="block-avantages">
					<div class="avantages-text">
						<p>
							L’environnement, la santé et l’économie sont des préoccupations majeures aujourd’hui. Le vélo est une solution à ces trois facteurs. Cependant, les boutiques de location de vélos se trouvent essentiellement à proximité des lieux touristiques. Pourtant, des vélos il y en a partout : nous sommes 66 millions de français et nous avons tous un vélo dans notre garage, notre cave… Grace à notre site trouve un vélo partout en France et à moindre coût ! Ce site est destiné à tout le monde : amateurs de balade à vélo, sportifs, citadins, business man, touristes… L’enjeu est de pouvoir trouver un vélo partout en France à un prix attractif tout en participant à la diminution des émissions de CO2 et faire du vélo un mode de transport à part entière.
						</p>
					</div>

					<div class="avantages-list">

						<div class="avantages-item item-economie">
							<div class="avantages-item-logo">100%</div>
							<div class="avantages-item-infos">
								<h2 class="avantages-item-title">économie</h2>
								<p class="avantages-item-description">Propriétaire, bénéficie d'un revenu supplémentaire. Locataire, économise de l'argent !</p>
							</div>
							
						</div>

						<div class="avantages-item item-ecologie">
							<div class="avantages-item-logo">100%</div>
							<div class="avantages-item-infos">
								<h2 class="avantages-item-title">écologie</h2>
								<p class="avantages-item-description">Réduis tes émissions de CO2 !</p>
							</div>
							
						</div>

						<div class="avantages-item item-sante">
							<div class="avantages-item-logo">100%</div>
							<div class="avantages-item-infos">
								<h2 class="avantages-item-title">santé</h2>
								<p class="avantages-item-description">Fais du sport tout en t'amusant !</p>
							</div>
							
						</div>
						
					</div>

					
					
				</div>
			</div>
		</section>

	</main>

	<?php require('footer.php'); ?>

</body>
</html>