<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>

		<section class="search-annonce">
			<div class="container">
				<div class="block-form-search">
					<form class="ui form">

						<div class="field">
							<label class="label-hidden">Que recherchez-vous ?</label>
							<input type="text" placeholder="Que recherchez-vous ?">
						</div>

						<div class="field">
							<label class="label-hidden">Catégorie</label>
							<select class="ui fluid dropdown">
								<option value="" disabled selected>Choisissez une catégorie</option>
								<option value="velo-ville">Vélo de ville</option>
								<option value="velo-electrique">Vélo électrique</option>
								<option value="tandem">Tandem</option>
								<option value="velo-route">Vélo de route</option>
								<option value="vtt">VTT</option>
								<option value="velo-enfant">Vélo enfant</option>
							</select>
						</div>

						<div class="field">
							<label class="label-hidden">Ville ou code postal</label>
							<div class="ui left icon input">
								<input type="text" placeholder="Ville ou code postal">
								<i class="map marker alternate icon icon-marker"></i>
							</div>	
						</div>

						<div class="field">
							<label class="label-hidden">Dans un rayon de</label>
							<select class="ui fluid dropdown">
								<option value="" disabled selected>Dans un rayon de</option>
								<option value="1">1 km</option>
								<option value="2">2 km</option>
								<option value="3">3 km</option>
								<option value="4">4 km</option>
								<option value="5">5 km</option>
								<option value="10">10 km</option>
								<option value="20">20 km</option>
								<option value="50">50 km</option>
								<option value="100">100 km</option>
								<option value="200">200 km</option>
								<option value="200+">200 km et plus</option>
							</select>
						</div>

						<button class="ui primary button" type="submit">Rechercher</button>
						
					</form>
				</div>
			</div>
		</section>

		<section class="list-annonces-result">
			<div class="container">
				<div class="list-annonces">

					<h2 class="list-annonces-title">Annonces</h2>

					<div class="block-list-annonces-found">

						<p class="number-annonces-found">X annonces</p>

						<ul class="list-annonces-found">
							<?php
								for ($i = 1; $i <= 6; $i++) 
								{ 
							?>
							<li class="block-annonce">
								<a href="annonce.php" class="link-annonce">
									<div class="block-img-annonce">
										<img class="img-annonce" src="img/iconphoto.png">
									</div>
									<div class="infos-annonce">
										<div class="titre-prix-annonce">
											<h4 class="titre-annonce">Titre de l'annonce</h4>
											<p class="prix-annonce">Prix de l'annonce € / jour</p>			
										</div>
										<div class="catgeory-lieu-annonce">
											<p class="category-annonce">Catégorie vélo</p>
											<p class="lieu-annonce">NomVille, CodePostal</p>
										</div>
									</div>
								</a>
							</li>
							<?php
								}
							?>

						</ul>

						<div class="loader-annonce">
							<div class="ui active inline large loader"></div>							
						</div>
						
					</div>

				</div>
			</div>
		</section>

	</main>

	<?php require('footer.php'); ?>

	<script type="text/javascript">
		$('.ui.dropdown').dropdown();
	</script>

</body>
</html>