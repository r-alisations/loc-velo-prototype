<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>

		<section class="remove-annonce">
			<div class="container">
				<div class="block-form-remove-annonce block-part-form">
					
					<h1 class="ui dividing header">Supprimer l'annonce</h1>

					<form class="ui form" action="suppressionannonce.php">

						<div class="field">
							<label>Email *</label>
							<input type="text" name="email" placeholder="Email">
						</div>

						<button class="ui button negative" type="submit">Supprimer l'annonce</button>

					</form>
				</div>
			</div>
		</section>

	</main>

</body>
</html>