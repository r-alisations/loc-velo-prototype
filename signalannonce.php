<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>

		<section class="signal-annonce">
			<div class="container">
				<div class="block-form-signal-annonce block-part-form">
					
					<h1 class="ui dividing header">Signaler l'annonce</h1>

					<form class="ui form" action="signalementannonce.php">
						
						<label for="motif">Motif *</label>

						<div class="inline fields">

							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="motif" tabindex="0" class="hidden">
									<label>Fraude</label>
								</div>
							</div>

							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="motif" tabindex="0" class="hidden">
									<label>Doublon</label>
								</div>
							</div>

							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="motif"  tabindex="0" class="hidden">
									<label>Mauvaise catégorie</label>
								</div>
							</div>

							<div class="field">
								<div class="ui radio checkbox">
									<input type="radio" name="motif" tabindex="0" class="hidden">
									<label>Autre abus</label>
								</div>
							</div>

						</div>

						<div class="field">
							<label>Nom *</label>
							<input type="text" name="nom" placeholder="Nom">
						</div>

						<div class="field">
							<label>Prénom *</label>
							<input type="text" name="prenom" placeholder="Prénom">
						</div>

						<div class="field">
							<label>Référence</label>
							<p>URL de l'annonce</p>
						</div>

						<div class="field">
							<label>Message *</label>
							<textarea placeholder="Message"></textarea>
						</div>

						<button class="ui button negative" type="submit">Signaler l'annonce</button>

					</form>
				</div>
			</div>
		</section>

	</main>

	<script src="https://code.jquery.com/jquery-3.4.0.js" integrity="sha256-DYZMCC8HTC+QDr5QNaIcfR7VSPtcISykd+6eSmBW5qo=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.js"></script>

	<script type="text/javascript">
		$('.ui.radio.checkbox').checkbox();
	</script>


</body>
</html>