<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>
		<section class="message-suppression">
			<div class="container">

				<div class="block-message">

					<h2 class="ui dividing header titre-suppression">Signalement de l'annonce</h2>

					<p class="paragraphe-suppression">
						<span class="paragraphe-suppression-span">Votre demande de signalement de l'annonce a bien été prise en compte.</span> 
						Nous vous remercions pour l'interet que vous portez à la conformité des annonces de notre site.
					</p>

					<div class="block-btn-message">
						<a class="ui primary basic button large" href="home.php">Retour à l'accueil</a>
					</div>
					
				</div>

			</div>
		</section>
	</main>

</body>
</html>