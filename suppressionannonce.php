<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>
		<section class="message-suppression">
			<div class="container">

				<div class="block-message">

					<h2 class="ui dividing header titre-suppression">Suppression de l'annonce</h2>

					<p class="paragraphe-suppression">
						<span class="paragraphe-suppression-span">Votre demande de suppression a bien été prise en compte.</span> 
						Votre annonce sera supprimée dans un délai de 45 minutes maximum.
					</p>

					<?php require('block-btn-message.php') ?>
					
				</div>

			</div>
		</section>
	</main>

</body>
</html>