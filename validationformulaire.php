<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php require('stylesheet.php'); ?>
</head>
<body>

	<?php require('header.php'); ?>

	<main>
		<section class="message-confirmation">
			<div class="container">

				<div class="block-message block-message-confirmation">

					<img class="image-confirmation" src="img/confirmation.png" alt="confirmation">
					
					<div class="block-text-message">
						<h2 class="titre-confirmation">Félicitations, nous avons bien reçu votre annonce !</h2>

						<p class="paragraphe-confirmation">
							Nous vous remercions de votre confiance. Nos services contrôleront votre annonce d’ici quelques instants.
							Elle apparaîtra parmi les annonces du site une fois contrôlée.
							Vous recevrez un email de validation une fois votre annonce en ligne.
						</p>	
					</div>
					
					<?php require('block-btn-message.php') ?>
					
				</div>

			</div>
		</section>
	</main>

</body>
</html>